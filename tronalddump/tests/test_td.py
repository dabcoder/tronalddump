import unittest
from tronalddump import TronaldDump


class TestTronalddump(unittest.TestCase):

    def test_random_quote_non_empty_string(self):
        self.td = TronaldDump()
        rq = self.td.randomquote()
        self.assertIsInstance(rq.quote, str)
        self.assertIsNotNone(rq.quote)
    
    def test_tags_not_empty(self):
        self.td = TronaldDump()
        tags = self.td.tags()
        self.assertTrue(isinstance(tags, list))
        self.assertTrue(len(tags) != 0)

    def test_search_returns_non_null(self):
        self.td = TronaldDump()
        sr = self.td.search("hillary")
        self.assertIsNotNone(sr)

    def test_search_tag_returns_non_null(self):
        self.td = TronaldDump()
        sr = self.td.searchtag("Hillary Clinton")
        self.assertIsNotNone(sr)
    
    def test_getquote_returns_quote(self):
        self.td = TronaldDump()
        gq = self.td.getquote("VHKwB8crTte7--FqtIxq9A")
        self.assertIsNotNone(gq)
        self.assertEqual(gq.quote_date, "2016-11-20T01:35:43.157523")

if __name__ == '__main__':
    unittest.main()